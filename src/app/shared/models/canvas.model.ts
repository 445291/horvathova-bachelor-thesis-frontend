import {RoleComponent} from "../../element/role/role.component";
import {LinkComponent} from "../../element/link/link.component";

/**
 * Model of a canvas.
 */
export class CanvasModel {
  idHead: number = 0;
  roles: RoleComponent[] = [];
  links: LinkComponent[] = [];

  /**
   * Counts IDs for a canvas so they're unique for each element.
   * @returns {number} current ID number and increase counter for the future
   */
  getAndIncrementId(): number {
    let current = this.idHead;
    this.idHead += 1;

    return current;
  }

  updateId(id: number) {
    if (this.idHead <= id) {
      this.idHead = id + 1;
    }
  }

  /**
   * Find a role on the canvas by ID.
   * @param {number} id   ID of a role to be found
   * @returns {any}       role, if found; null otherwise
   */
  findRole(id: number) {
    for (let role of this.roles) {
      if (role.getModel().object_id == id) {
        return role;
      }
    }

    return null;
  }

  /**
   * Clears the canvas.
   */
  clear() {
    for (let role of this.roles) {
      role.ref.destroy();
    }

    for (let link of this.links) {
      link.ref.destroy();
    }

    this.idHead = 0;
    this.roles = [];
    this.links = [];
  }

  removeRole(role) {
    let index = this.roles.indexOf(role);

    if (index > -1) {
      this.roles.splice(index, 1);
    }
  }

  removeLink(link) {
    let index = this.links.indexOf(link);

    if (index > -1) {
      this.links.splice(index, 1);
    }
  }

  getRoles() {
    return this.roles;
  }
}
