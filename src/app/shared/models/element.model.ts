/**
 * Model of elements.
 */
export class ElementModel {
  object_id: number;

  saveToDict(dict) {
    dict['object_id'] = this.object_id;
  }

  loadFromDict(dict) {
    this.object_id = dict.object_id;
  }

  isValid(): boolean {
    return true;
  }
}
