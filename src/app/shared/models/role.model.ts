import {ElementModel} from "./element.model";

/**
 * Model of roles.
 */
export class RoleModel extends ElementModel {
  object_id: number;
  x: number;
  y: number;
  data_name: string;
  data_ip: string;

  saveToDict(dict) {
    super.saveToDict(dict);
    dict['x'] = this.x;
    dict['y'] = this.y;
    dict['data_name'] = this.data_name;
    dict['data_ip'] = this.data_ip;
  }

  loadFromDict(dict) {
    super.loadFromDict(dict);
    this.x = dict.x;
    this.y = dict.y;
    this.data_name = dict.data_name;
    this.data_ip = dict.data_ip;
  }

  /**
   * Checks if required field for a role where filled in and if they are in correct form.
   * @returns {boolean}   true, if a role is valid; false, otherwise
   */
  isValid(): boolean {
    if (!this.data_name) return false;
    if (!this.data_ip) return false;
    if (!this.data_ip.match('\\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|$)){4}\\b')) return false;

    return super.isValid();
  }
}
