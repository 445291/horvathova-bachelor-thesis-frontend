/**
 * Model of a graph.
 */
export class Graph {
  id: number;
  name: string;
  roles: object[];
  links: object[];
}
