import {RoleModel} from "./role.model";

/**
 * Model of hosts.
 */
export class HostModel extends RoleModel {
  data_role: string;
  data_vcn_password: string;
  data_image_id: number;
  data_group: string;
  data_note: string;

  saveToDict(dict): void {
    super.saveToDict(dict);
    dict['data_role'] = this.data_role;
    dict['data_vcn_password'] = this.data_vcn_password;
    dict['data_image_id'] = this.data_image_id;
    dict['data_group'] = this.data_group;
    dict['data_note'] = this.data_note;
  }

  loadFromDict(dict): void {
    super.loadFromDict(dict);
    this.data_role = dict.data_role;
    this.data_vcn_password = dict.data_vcn_password;
    this.data_image_id = dict.data_image_id;
    this.data_group = dict.data_group;
    this.data_note = dict.data_note;
  }
}
