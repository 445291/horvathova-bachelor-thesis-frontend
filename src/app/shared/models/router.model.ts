import {RoleModel} from "./role.model";

/**
 * Model of routers.
 */
export class RouterModel extends RoleModel {
  data_prefix: number;
  data_image_id: number;
  data_note: string;

  saveToDict(dict): void {
    super.saveToDict(dict);
    dict['data_prefix'] = this.data_prefix;
    dict['data_image_id'] = this.data_image_id;
    dict['data_note'] = this.data_note;
  }

  loadFromDict(dict): void {
    super.loadFromDict(dict);
    this.data_prefix = dict.data_prefix;
    this.data_image_id = dict.data_image_id;
    this.data_note = dict.data_note;
  }

  /**
   * Checks if required field specific for a router where filled in and if they are in a correct form.
   * @returns {boolean}   true, if a router is valid; false, otherwise
   */
  isValid(): boolean {
    if (!this.data_prefix) return false;
    if (!String(this.data_prefix).match('(^[1-9]$)|(^[1-2][0-9]$)|(^30$)')) return false;

    return super.isValid();
  }
}
