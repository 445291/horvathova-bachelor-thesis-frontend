import {ElementModel} from "./element.model";

/**
 * Model of links.
 */
export class LinkModel extends ElementModel {
  object_id: number;
  source_id: number;
  target_id: number;
  data_note: string;

  saveToDict(dict): void {
    super.saveToDict(dict);
    dict['source_id'] = this.source_id;
    dict['target_id'] = this.target_id;
    dict['data_note'] = this.data_note;
  }

  loadFromDict(dict): void {
    super.loadFromDict(dict);
    this.source_id = dict.source_id;
    this.target_id = dict.target_id;
    this.data_note = dict.data_note;
  }
}
