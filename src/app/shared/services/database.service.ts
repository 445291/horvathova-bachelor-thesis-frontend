import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {Observable} from "rxjs/Observable";
import {catchError} from 'rxjs/operators';
import {of} from "rxjs/observable/of";
import {Subject} from "rxjs/Subject";

import {Graph} from "../models/graph.model";
import {CanvasModel} from "../models/canvas.model";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

/**
 * A service for a communication with a backend server.
 */
@Injectable()
export class DatabaseService {
  constructor(private http: HttpClient) {
  }

  // URL to REST API server
  public static graphsUrl = 'https://sbarp-backend.herokuapp.com/rest';

  private allGraphsData = new Subject();
  private graphData = new Subject();
  private jsonDownloadGraphData = new Subject();

  /**
   * Sends GET request at '/rest/all'.
   * Assigns received data to 'allGraphsData' attribute, that is Observable.
   * Used for obtaining a list of all graphs.
   * @param {Function} callbackError    error if request is unsuccessful
   * @returns {Subscription}
   */
  getAllGraphs(callbackError: Function = null) {
    return this.http.get<Graph[]>(`${DatabaseService.graphsUrl}/all`).subscribe(
      data => {
        this.allGraphsData.next(data);
      },
      response => {
        if (callbackError) {
          callbackError(response);
        }
      },
      () => {
      });
  }

  /**
   * Sends GET request at '/rest/detail'.
   * Assigns received data to 'graphData' attribute, that is Observable.
   * Used for loading a graph.
   * @param {string} filename     name of a graph which data we are requesting
   */
  loadGraph(filename: string) {
    let url = `${DatabaseService.graphsUrl}/detail`;
    let params = new HttpParams().set("filename", filename);

    this.http.get<Graph>(url, {params: params}).pipe(
      catchError(this.handleError<Graph>('getGraph id=${id}'))
    ).subscribe(
      data => {
        this.graphData.next(data);
      });
  }

  /**
   * Sends GET request at '/rest/detail'.
   * Assigns received data to 'jsonDownloadGraphData' attribute, that is Observable.
   * Used for downloading a graph as JSON.
   * @param {string} filename     name of a graph which data we are requesting
   */
  downloadJSON(filename: string) {
    let url = `${DatabaseService.graphsUrl}/detail`;
    let params = new HttpParams().set("filename", filename);

    this.http.get<Graph>(url, {params: params}).pipe(
      catchError(this.handleError<Graph>('getGraph id=${id}'))
    ).subscribe(
      data => {
        this.jsonDownloadGraphData.next(data);
      });
  }

  /**
   * Sends POST request at '/rest/all'.
   * Used for saving a graph.
   * @param {string} filename           name of a graph to be saved under
   * @param {CanvasModel} model         model of a graph
   * @param {Function} callbackSuccess
   * @param {Function} callbackError
   */
  saveGraph(filename: string, model: CanvasModel, callbackSuccess: Function = null, callbackError: Function = null) {
    let rolesOutput = [];

    for (let role of model.roles) {
      rolesOutput.push(role.toJSON())
    }

    let linksOutput = [];

    for (let link of model.links) {
      linksOutput.push(link.toJSON())
    }
    let url = `${DatabaseService.graphsUrl}/all`;

    this.http.post(url, {
      name: filename,
      roles: rolesOutput,
      links: linksOutput
    }).subscribe(
      (val) => {
        this.getAllGraphs();

        if (callbackSuccess) {
          callbackSuccess();
        }
      },
      response => {
        if (callbackError) {
          callbackError(response);
        }
      },
      () => {
      });
  }

  /**
   * Sends DELETE request at '/rest/all'.
   * Used for deleting a graph.
   * @param {string} filename     name of a graph to be deleted
   */
  deleteGraph(filename: string) {
    let url = `${DatabaseService.graphsUrl}/all`;
    let params = new HttpParams().set("filename", filename);

    this.http.delete(url, {params: params}).subscribe(
      (val) => {
        console.log('DELETE call successful value returned in body', val);
        this.getAllGraphs();
      },
      response => {
        console.log('DELETE call in error', response);
      },
      () => {
      });
  }

  /**
   * Sends PATCH request at '/rest/detail'.
   * Used for renaming an existing graph.
   * @param {string} filename           old name of a graph to be renamed
   * @param {string} newFilename        new name for a graph
   * @param {Function} callbackSuccess
   * @param {Function} callbackError
   */
  renameGraph(filename: string, newFilename: string, callbackSuccess: Function = null, callbackError: Function = null) {
    let url = `${DatabaseService.graphsUrl}/detail`;

    this.http.patch<Graph>(url, {filename: filename, new_filename: newFilename}).subscribe(
      (data) => {
        if (callbackSuccess) {
          callbackSuccess();
        }
      },
      response => {
        if (callbackError) {
          callbackError(response);
        }
      },
      () => {
      });
  }

  getAllGraphsDataObservable() {
    return this.allGraphsData;
  }

  getGraphDataObservable() {
    return this.graphData;
  }

  getJsonDownloadGraphDataObservable() {
    return this.jsonDownloadGraphData;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
