import {DatabaseService} from "./database.service";
import {async} from "@angular/core/testing";
import {HttpParams} from "@angular/common/http";
import {Subject} from "rxjs/Subject";
import {CanvasModel} from "../models/canvas.model";

describe('DatabaseService', () => {
  let service;
  let httpClientSpy;
  let baseUrl = DatabaseService.graphsUrl;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete', 'patch']);
    service = new DatabaseService(<any> httpClientSpy);

    httpClientSpy.get.and.returnValue(new Subject());
    httpClientSpy.post.and.returnValue(new Subject());
    httpClientSpy.delete.and.returnValue(new Subject());
    httpClientSpy.patch.and.returnValue(new Subject());
  });

  it('should call REST API GET for all graphs', async(() => {
    service.getAllGraphs();

    expect(httpClientSpy.get.calls.count()).toBe(1);
    expect(httpClientSpy.get.calls.mostRecent().args[0]).toBe(`${baseUrl}/all`);
  }));

  it('should call REST API GET for graph detail', async(() => {
    service.loadGraph('test_name');

    expect(httpClientSpy.get.calls.count()).toBe(1);
    expect(httpClientSpy.get.calls.mostRecent().args[0]).toBe(`${baseUrl}/detail`);
    expect((httpClientSpy.get.calls.mostRecent().args[1].params as HttpParams).get('filename'))
      .toBe('test_name');

    service.downloadJSON('test_name_2');

    expect(httpClientSpy.get.calls.count()).toBe(2);
    expect(httpClientSpy.get.calls.mostRecent().args[0]).toBe(`${baseUrl}/detail`);
    expect((httpClientSpy.get.calls.mostRecent().args[1].params as HttpParams).get('filename'))
      .toBe('test_name_2');
  }));

  it('should call REST API POST for graph saving', async(() => {
    const role1 = jasmine.createSpyObj('RoleComponent', ['toJSON']);
    role1.toJSON.and.returnValue('role1');
    const role2 = jasmine.createSpyObj('RoleComponent', ['toJSON']);
    role2.toJSON.and.returnValue('role2');
    const link1 = jasmine.createSpyObj('LinkComponent', ['toJSON']);
    link1.toJSON.and.returnValue('link1');
    const link2 = jasmine.createSpyObj('LinkComponent', ['toJSON']);
    link2.toJSON.and.returnValue('link2');

    const model = new CanvasModel();
    model.roles.push(role1);
    model.roles.push(role2);
    model.links.push(link1);
    model.links.push(link2);

    service.saveGraph('test_name', model);

    expect(httpClientSpy.post.calls.count()).toBe(1);
    expect(httpClientSpy.post.calls.mostRecent().args[0]).toBe(`${baseUrl}/all`);
    expect(httpClientSpy.post.calls.mostRecent().args[1].name).toBe('test_name');
    expect(httpClientSpy.post.calls.mostRecent().args[1].roles[0]).toBe('role1');
    expect(httpClientSpy.post.calls.mostRecent().args[1].roles[1]).toBe('role2');
    expect(httpClientSpy.post.calls.mostRecent().args[1].links[0]).toBe('link1');
    expect(httpClientSpy.post.calls.mostRecent().args[1].links[1]).toBe('link2');
  }));

  it('should call REST API DELETE for graph deletion', async(() => {
    service.deleteGraph('test_name');

    expect(httpClientSpy.delete.calls.count()).toBe(1);
    expect(httpClientSpy.delete.calls.mostRecent().args[0]).toBe(`${baseUrl}/all`);
    expect((httpClientSpy.delete.calls.mostRecent().args[1].params as HttpParams).get('filename'))
      .toBe('test_name');
  }));

  it('should call REST API PATCH for graph renaming', async(() => {
    service.renameGraph('old_name', 'new_name');


    expect(httpClientSpy.patch.calls.count()).toBe(1);
    expect(httpClientSpy.patch.calls.mostRecent().args[0]).toBe(`${baseUrl}/detail`);
    expect(httpClientSpy.patch.calls.mostRecent().args[1].filename).toBe('old_name');
    expect(httpClientSpy.patch.calls.mostRecent().args[1].new_filename).toBe('new_name');
  }));
});
