import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CdkTableModule} from '@angular/cdk/table';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule, MatInputModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from "@angular/forms";
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatTabsModule} from '@angular/material/tabs';
import {FlexLayoutModule} from '@angular/flex-layout';

import {AppComponent} from './app.component';
import {LinkComponent} from "./element/link/link.component";
import {DesktopComponent} from "./element/role/host/desktop";
import {MobileComponent} from "./element/role/host/mobile";
import {ServerComponent} from "./element/role/host/server";
import {RouterComponent} from "./element/role/router/router";
import {DatabaseService} from "./shared/services/database.service";
import {RouterFormComponent} from "./sidebar/forms/router-form.component";
import {HostFormComponent} from "./sidebar/forms/host-form.component";
import {LinkFormComponent} from "./sidebar/forms/link-form.component";
import {RoleDetailComponent} from "./sidebar/role-detail.component";
import {TabCanvasComponent} from "./tab/tab-canvas.component";
import {TabPersistenceComponent} from "./tab/tab-persistence.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AppModule} from "./app.module";

@NgModule({
  declarations: [
  ],
  imports: [
    AppModule,
    HttpClientTestingModule,
  ],
  providers: [
    DatabaseService,
    TabCanvasComponent,
  ],
  bootstrap: [
    AppComponent,
  ],
  entryComponents: [
    LinkComponent,
    DesktopComponent,
    MobileComponent,
    ServerComponent,
    RouterComponent,
    RouterFormComponent,
    HostFormComponent,
    LinkFormComponent,
    TabPersistenceComponent,
    TabCanvasComponent,
    RoleDetailComponent,
  ]
})

export class TestModule {
}
