import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from "@angular/core";

import {TabCanvasComponent} from "../tab/tab-canvas.component";
import {RouterFormComponent} from "./forms/router-form.component";
import {LinkFormComponent} from "./forms/link-form.component";
import {HostFormComponent} from "./forms/host-form.component";
import {LinkComponent} from "../element/link/link.component";
import {RouterComponent} from "../element/role/router/router";

@Component({
  selector: 'role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./forms/form.component.css']

})

/**
 * A component for a sidebar.
 */
export class RoleDetailComponent implements OnInit {
  constructor(private cfr: ComponentFactoryResolver) {
  }

  @ViewChild('form', {read: ViewContainerRef}) containerForm: ViewContainerRef;

  selectedElement: any = null;
  formComponent: any = null;
  parentCanvas: TabCanvasComponent = null;

  ngOnInit() {
  }

  /**
   * Show details and a form of a selected element.
   * @param comp    a component of which details will be shown
   */
  showDetails(comp) {
    this.selectedElement = comp;

    if (this.formComponent != null) {
      this.formComponent.destroy();
      this.formComponent = null;
    }

    if (this.selectedElement != null) {
      let componentFactory = null;

      if (this.selectedElement.constructor == RouterComponent) {
        componentFactory = this.cfr.resolveComponentFactory(RouterFormComponent);
      } else if (this.selectedElement.constructor == LinkComponent) {
        componentFactory = this.cfr.resolveComponentFactory(LinkFormComponent);
      } else {
        componentFactory = this.cfr.resolveComponentFactory(HostFormComponent);
      }

      let component = <any>this.containerForm.createComponent(componentFactory);

      component.instance.setModel(this.selectedElement.getModel());
      component.instance.onModelChangeCallback = this.onModelChange.bind(this);

      this.formComponent = component;
    }
  }

  onModelChange() {
    this.parentCanvas.isModified = true;

    if (!this.selectedElement.isValid) {
      this.selectedElement.isValid = this.selectedElement.getModel().isValid();
    }
  }
}
