import {Component} from "@angular/core";

import {FormComponent} from "./form.component";

/**
 * A component for a form of a host role.
 */
@Component({
  templateUrl: './host-form.component.html',
  styleUrls: ['./form.component.css']
})
export class HostFormComponent extends FormComponent {
}
