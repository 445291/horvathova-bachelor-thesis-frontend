import {Component} from "@angular/core";

import {FormComponent} from "./form.component";

/**
 * A component for a form of a link.
 */
@Component({
  templateUrl: './link-form.component.html',
  styleUrls: ['./form.component.css']
})
export class LinkFormComponent extends FormComponent {
}
