import {Component} from "@angular/core";

import {FormComponent} from "./form.component";

/**
 * A component for a form of a router role.
 */
@Component({
  templateUrl: './router-form.component.html',
  styleUrls: ['./form.component.css']
})
export class RouterFormComponent extends FormComponent {
}
