/**
 * A parent component for a form.
 */
export class FormComponent {
  model = null;
  onModelChangeCallback: Function = null;

  setModel(model) {
    this.model = model;
  }

  onModelChange() {
    if (this.onModelChangeCallback != null) {
      this.onModelChangeCallback();
    }
  }
}
