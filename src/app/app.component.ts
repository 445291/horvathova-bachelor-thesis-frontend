import {Component, ComponentFactoryResolver, HostListener, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import * as fileSaver from "file-saver";

import {TabCanvasComponent} from "./tab/tab-canvas.component";
import {TabPersistenceComponent} from "./tab/tab-persistence.component";
import {DatabaseService} from "./shared/services/database.service";

/**
 * A main component of an application.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor(private cfr: ComponentFactoryResolver,
              private databaseService: DatabaseService) {
  }

  @ViewChild('tabContent', {read: ViewContainerRef}) tabContent: ViewContainerRef;

  components = [];
  selectedTab = 0;

  /**
   * On initialization adds persistence tab.
   * Observer for DatabaseService attributes:
   *    graphData - opens tab with graph
   *    jsonDownloadGraphData - saves graph as JSON, uses library file-saver
   */
  ngOnInit() {
    this.addPersistenceTab();
    this.onTabChange(this.selectedTab);

    this.databaseService.getGraphDataObservable().subscribe(data => {
      let filename = (data as any).name;
      let existingTabIndex = null;

      for (let tabIndex = 1; tabIndex < this.components.length; tabIndex++) {
        if (this.components[tabIndex].instance.filename == filename) {
          existingTabIndex = tabIndex;
          break;
        }
      }

      if (existingTabIndex) {
        this.onTabChange(existingTabIndex)
      } else {
        let component = this.addCanvasTab(filename);
        component.instance.load(data);
        this.onTabChange(this.components.length - 1);
      }
    });

    this.databaseService.getJsonDownloadGraphDataObservable().subscribe(data => {
      let blob = new Blob([JSON.stringify(data)], {type: 'text/json'});

      fileSaver.saveAs(blob, (data as any).name + ".json");
    });
  }

  /**
   * Adds a persistence tab.
   * Called only once during initialization.
   * @returns {any} PersistenceTabComponent
   */
  addPersistenceTab() {
    let componentFactory = this.cfr.resolveComponentFactory(TabPersistenceComponent);
    let component = <any>this.tabContent.createComponent(componentFactory);

    this.components.push(component);

    return component;
  }

  /**
   * Adds a new canvas tab with a name set as label.
   * If no name set, label will be marked as 'new_graph'.
   * @param name    name of a graph to be set as a tab label
   * @returns {any} CanvasTabComponent
   */
  addCanvasTab(name) {
    let componentFactory = this.cfr.resolveComponentFactory(TabCanvasComponent);
    let component = <any>this.tabContent.createComponent(componentFactory);

    component.instance.filename = name;
    this.components.push(component);

    return component;
  }

  /**
   * Hides content of all tabs, besides the one, that was changed for an active one.
   * If the last one was clicked, creates a new canvas tab.
   * @param tabIndex
   */
  onTabChange(tabIndex) {
    this.selectedTab = tabIndex;

    if (tabIndex == this.components.length) {
      this.addCanvasTab(null);
    }

    for (let comp of this.components) {
      if (comp != null) {
        comp.instance.isHidden = true;
      }
    }

    if (this.components[tabIndex] != null) {
      this.components[tabIndex].instance.isHidden = false;
    }
  }

  /**
   * Deletes tab from a header.
   * If unsaved changes, reassures if an action should really happen.
   * @param event
   * @param tabIndex  index of a tab to be deleted
   */
  deleteTab(event, tabIndex) {
    let comp = this.components[tabIndex];

    if (!comp.instance.isModified || window.confirm('Are you sure you want to close this tab? You will loose unsaved changes.')) {
      let removedComp = this.components.splice(tabIndex, 1);

      removedComp[0].destroy();

      if (tabIndex <= this.selectedTab) {
        this.onTabChange(this.selectedTab - 1);
      }
    }

    if (event) {
      event.preventDefault();
    }
  }

  /**
   * When there is an unsaved change in an app,
   *  reassures for an action to happen,
   *  when browser tab is going to be closed or reload.
   * @param event
   */
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    let isUnsaved = false;

    for (let tabIndex = 1; tabIndex < this.components.length; tabIndex++) {
      if (this.components[tabIndex].instance.isModified) {
        isUnsaved = true;
        break;
      }
    }

    if (isUnsaved) {
      event.returnValue = 'Are you sure you want to close this window? You will loose unsaved changes.';
    }
  }
}
