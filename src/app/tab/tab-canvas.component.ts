import {AfterViewInit, Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';

import * as svgPanZoom from "svg-pan-zoom"

import {DatabaseService} from "../shared/services/database.service";
import {CanvasModel} from "../shared/models/canvas.model";
import {MobileComponent} from "../element/role/host/mobile";
import {DesktopComponent} from "../element/role/host/desktop";
import {ServerComponent} from "../element/role/host/server";
import {RouterComponent} from "../element/role/router/router";
import {LinkComponent} from "../element/link/link.component";
import {HostComponent} from "../element/role/host/host";
import {RoleDetailComponent} from "../sidebar/role-detail.component";

/**
 * A component of a canvas tab.
 */
@Component({
  selector: 'tab-canvas',
  templateUrl: './tab-canvas.component.html',
  styleUrls: ['./tab-canvas.component.css']
})

export class TabCanvasComponent implements OnInit, AfterViewInit {
  constructor(private cfr: ComponentFactoryResolver,
              private databaseService: DatabaseService) {
  }

  @ViewChild('roles', {read: ViewContainerRef}) containerRoles: ViewContainerRef;
  @ViewChild('links', {read: ViewContainerRef}) containerLinks: ViewContainerRef;
  @ViewChild('temporaryLink', {read: ViewContainerRef}) containerTemporaryLink: ViewContainerRef;
  @ViewChild('roleDetail', {read: ViewContainerRef}) containerRoleDetail: ViewContainerRef;
  @ViewChild('canvas') containerCanvas: ViewContainerRef;

  filename: string = null;
  model: CanvasModel = new CanvasModel();
  roleDetailComponent: RoleDetailComponent = null;
  svgControls: any = null;
  errorMessage: string = null;

  currentX = 0;
  currentY = 0;

  roleClicked = null;
  selectedElement = null;
  selectedElementInMotion = null;
  selectedFirst = null;
  tempLink = null;

  isHidden = true;
  isModified = false;
  mouseDownOnCanvas: boolean = false;

  ngOnInit() {
    let componentClass = this.cfr.resolveComponentFactory(RoleDetailComponent);
    let component = this.containerRoleDetail.createComponent(componentClass);
    this.roleDetailComponent = component.instance;
    this.roleDetailComponent.parentCanvas = this;
  }

  /**
   * Lifecycle hook in which canvas zoom and pan are set up. Uses svg-pan-zoon library.
   */
  ngAfterViewInit(): void {
    this.svgControls = svgPanZoom((this.containerCanvas as any).nativeElement, {
      panEnabled: true,
      controlIconsEnabled: true,
      zoomEnabled: true,
      dblClickZoomEnabled: false,
      mouseWheelZoomEnabled: true,
      preventMouseEventsDefault: true,
      zoomScaleSensitivity: 0.2,
      minZoom: 0.4,
      maxZoom: 1.5,
      fit: false,
      contain: false,
      center: true,
      refreshRate: 'auto',
      beforePan: function (oldPan, newPan) {
        let gutterWidth = 200;
        let gutterHeight = 200;
        let sizes = this.getSizes();
        let leftLimit = gutterWidth;
        let rightLimit = sizes.width - gutterWidth - (sizes.viewBox.width * sizes.realZoom);
        let topLimit = gutterHeight;
        let bottomLimit = sizes.height - gutterHeight - (sizes.viewBox.height * sizes.realZoom);
        let customPan = {
          x: 0,
          y: 0
        };
        customPan.x = Math.min(leftLimit, Math.max(rightLimit, newPan.x));
        customPan.y = Math.min(topLimit, Math.max(bottomLimit, newPan.y));
        return customPan;
      },
      beforeZoom: function (oldZoom, newZoom) {
      },
      eventsListenerElement: null,
    });
    this.svgControls.zoom(1);
  }

  dismissError() {
    this.errorMessage = null;
  }

  mouseUp() {
    this.selectedFirst = null;
    this.selectedElementInMotion = null;

    if (this.tempLink) {
      this.stopConnecting();
    } else {
      if (this.mouseDownOnCanvas && (this.selectedElement && !this.roleClicked)) {
        this.deselectComponent();
      }
    }

    this.mouseDownOnCanvas = false;
  }

  mouseDown() {
    this.mouseDownOnCanvas = true;
  }

  /**
   * Returns a x coordinate depending on a canvas zoom.
   * @param screenX     coordinate x
   * @returns {number}  calculated new relative coordinated x
   */
  getCanvasMouseX(screenX) {
    let zoom = this.svgControls.getSizes().realZoom;
    return (-this.svgControls.getPan().x + (screenX - 80)) / zoom;
  }

  /**
   * Returns a y coordinate depending on a canvas zoom.
   * @param screenY     coordinate y
   * @returns {number}  calculated new relative coordinated y
   */
  getCanvasMouseY(screenY) {
    let zoom = this.svgControls.getSizes().realZoom;
    return (-this.svgControls.getPan().y + (screenY - 48)) / zoom;
  }

  /**
   * Create a node on a canvas.
   * @param event
   */
  create(event) {
    if (this.roleClicked != null) {
      let componentClass = TabCanvasComponent.getClassFromName(this.roleClicked);
      let component = this.createComponent(componentClass);

      component.setCenter(this.getCanvasMouseX(event.x), this.getCanvasMouseY(event.y));
      this.checkCollisions(component);

      this.roleClicked = null;
      this.selectComponent(component);
    }
  }

  /**
   * Creates a role and sets all callbacks.
   * @param componentClass    class of a component to be created
   * @param {any} id          id of a component, if a new one, id is null
   * @returns {LinkComponent}   instance of a role
   */
  createComponent(componentClass, id = null) {
    this.isModified = true;

    let componentFactory = this.cfr.resolveComponentFactory(componentClass);
    let component = <any>this.containerRoles.createComponent(componentFactory);

    component.instance.ref = component;

    if (id == null) {
      component.instance.getModel().object_id = this.model.getAndIncrementId();
    } else {
      component.instance.getModel().object_id = id;
      this.model.updateId(id);
    }

    component.instance.callbackMouseDown = this.componentMouseDown.bind(this);
    component.instance.callbackMouseUp = this.componentMouseUp.bind(this);
    component.instance.callbackStartConnecting = this.componentStartConnecting.bind(this);
    component.instance.callbackDelete = this.deleteComponent.bind(this);

    this.model.roles.push(component.instance);

    return component.instance;
  }

  /**
   * Creates a link.
   * @param from   a node it starts from
   * @param to     a node it ends in
   * @param {any} id  id of a link, if a new one, id is null
   * @returns {LinkComponent}   instance of a link
   */
  createLink(from, to, id = null) {
    this.isModified = true;

    let linkComponentFactory = this.cfr.resolveComponentFactory(LinkComponent);
    let linkComponent = this.containerLinks.createComponent(linkComponentFactory);

    linkComponent.instance.ref = linkComponent;

    if (id == null) {
      linkComponent.instance.getModel().object_id = this.model.getAndIncrementId();
    } else {
      linkComponent.instance.getModel().object_id = id;
      this.model.updateId(id);
    }

    linkComponent.instance.getModel().source_id = from.getModel().object_id;
    linkComponent.instance.getModel().target_id = to.getModel().object_id;
    linkComponent.instance.callbackClick = this.linkClick.bind(this);
    linkComponent.instance.callbackDelete = this.deleteLink.bind(this);

    from.addOutboundLink(linkComponent.instance);
    to.addInboundLink(linkComponent.instance);

    this.model.links.push(linkComponent.instance);

    return linkComponent.instance;
  }

  /**
   * Deletes a role.
   * @param comp    a role to be deleted
   */
  deleteComponent(comp) {
    this.isModified = true;

    for (let link of Object.assign([], comp.outboundLinks)) {
      link.removeLink();

      this.deleteLink(link);
      this.model.removeLink(link);
    }

    for (let link of Object.assign([], comp.inboundLinks)) {
      link.removeLink();

      this.deleteLink(link);
      this.model.removeLink(link);
    }

    this.model.removeRole(comp);

    comp.ref.destroy();
  }

  /**
   * Deletes a link.
   * @param link  a link to be deleted
   */
  deleteLink(link) {
    this.isModified = true;

    let role = this.model.findRole(link.getModel().source_id);
    let index = role.outboundLinks.indexOf(link);

    if (index > -1) {
      role.outboundLinks.splice(index, 1);
    }

    role = this.model.findRole(link.getModel().target_id);
    index = role.inboundLinks.indexOf(link);

    if (index > -1) {
      role.inboundLinks.splice(index, 1);
    }

    this.model.removeLink(link);
  }

  /**
   * Selects a element.
   * @param comp  an element to be selected
   */
  selectComponent(comp) {
    if (this.selectedElement && this.selectedElement == comp) {
      this.deselectComponent();
    } else {
      this.deselectComponent();

      this.selectedElement = comp;
      this.selectedElement.highlight();

      this.roleDetailComponent.showDetails(comp);
    }
  }

  /**
   * Deselects a selected element.
   */
  deselectComponent() {
    if (this.selectedElement) {
      this.selectedElement.dehighlight();
      this.selectedElement = null;
      this.stopConnecting();

      this.roleDetailComponent.showDetails(null);
    }
  }

  /**
   * Selectes node for a moving.
   * While creating a link, nodes can not be moved.
   * @param comp    a node to be moved
   * @param event
   */
  selectComponentForMoving(comp, event) {
    if (!this.tempLink) {
      this.selectedElementInMotion = comp;
      this.currentX = event.clientX;
      this.currentY = event.clientY;
    }
  }

  /**
   * Updates coordinates and checks collisions when a node is moved.
   * @param event
   */
  mouseMoved(event) {
    if (this.selectedElementInMotion != null) {
      let dx = event.clientX - this.currentX;
      let dy = event.clientY - this.currentY;

      this.isModified = true;
      let sizes = this.svgControls.getSizes();
      this.selectedElementInMotion.moveBy(dx / sizes.realZoom, dy / sizes.realZoom);
      this.checkCollisions(this.selectedElementInMotion);

      this.currentX = event.clientX;
      this.currentY = event.clientY;
    }

    if (this.tempLink) {
      this.tempLink.updateEnd(this.getCanvasMouseX(event.x), this.getCanvasMouseY(event.y), null);
    }
  }

  /**
   * Check collisions of a node in a motion.
   * If moving node intersect with another node,
   *  touched node moves accordingly and checks its collisions as well.
   * @param elementInMotion   a node that is being dragged
   */
  checkCollisions(elementInMotion) {
    let checkAgainst = elementInMotion;

    for (let i = 0; i < this.model.getRoles().length; i++) {
      let role = this.model.getRoles()[i];

      if (role == checkAgainst) {
        continue;
      }

      let vectX = role.getCenterX() - checkAgainst.getCenterX();
      let vectY = role.getCenterY() - checkAgainst.getCenterY();
      let range = role.getRadius() + checkAgainst.getRadius();
      let vectLength = Math.sqrt(vectX * vectX + vectY * vectY);
      let space = 10;

      if (vectLength < range + space - 0.5) {
        vectX /= vectLength;
        vectY /= vectLength;
        vectX *= range + space;
        vectY *= range + space;

        role.setCenter(
          checkAgainst.getCenterX() + vectX,
          checkAgainst.getCenterY() + vectY
        );

        this.checkCollisions(role);
      }
    }
  }

  /**
   * Creates a temporary link for linking nodes that follows a cursor.
   * Disallowes linking for roled that selected role can not be linked with.
   * @param comp    a node we started linking from
   * @param event
   */
  componentStartConnecting(comp, event) {
    this.selectedFirst = comp;

    let linkComponentFactory = this.cfr.resolveComponentFactory(LinkComponent);
    let linkComponent = this.containerTemporaryLink.createComponent(linkComponentFactory);

    linkComponent.instance.ref = linkComponent;
    linkComponent.instance.updateStart(comp.getCenterX(), comp.getCenterY(), comp);
    linkComponent.instance.updateEnd(this.getCanvasMouseX(event.x), this.getCanvasMouseY(event.y), null);

    this.tempLink = linkComponent.instance;
    this.selectComponent(comp);

    // Tag disallowed components.
    for (let role of this.model.roles) {
      role.linkingDisallowed = !this.isValidForLinking(comp, role);
    }
  }

  stopConnecting() {
    if (this.tempLink) {
      this.tempLink.removeLink();
      this.tempLink = null;
    }

    for (let role of this.model.roles) {
      role.stopConnecting();
      role.linkingDisallowed = false;
    }
  }

  componentMouseUp(comp, event, click: boolean) {
    if (click) {
      if (!this.selectedFirst) {
        this.selectComponent(comp);
      } else {
        if (this.selectedFirst && (this.selectedFirst != comp) && this.isValidForLinking(this.selectedFirst, comp)) {
          this.createLink(this.selectedFirst, comp);
          this.selectedFirst = null;
        } else {
          this.selectedFirst = null;
        }

        if (this.tempLink) {
          this.stopConnecting();
        }
      }
    }

    this.selectedElementInMotion = null;
  }

  componentMouseDown(comp, event) {
    this.componentMouseUp(comp, event, false);
    this.selectComponentForMoving(comp, event);
  }

  linkClick(comp, event) {
    this.selectComponent(comp);
  }

  /**
   * If graph is validated, calls 'saveGraph' method of DatabaseService.
   * If it is a new graph, it asks for a name by a prompt window.
   */
  save() {
    this.dismissError();

    if (this.isValidForSaving()) {
      if (this.filename == null || this.filename == '') {
        let filename = window.prompt('Please enter graph name:',);

        if (filename != null && filename != '') {
          this.filename = filename
        }
      }

      if (this.filename != null && this.filename != '') {
        this.databaseService.saveGraph(this.filename, this.model,
          function () {
            this.isModified = false;
          }.bind(this),
          function (response) {
            this.errorMessage = response.error.name.join(' ');
          }.bind(this));
      }
    }
  }

  /**
   * Loads a graph on a canvas from a data.
   * @param data  data of a grapf to be loaded
   */
  load(data) {
    this.filename = null;
    this.model.clear();

    this.filename = data.name;

    for (let role of data.roles) {
      let componentClass = TabCanvasComponent.getClassFromName(role.class_name);
      let component = <any>this.createComponent(componentClass, role.object_id);
      component.loadJSON(role);
    }

    for (let link of data.links) {
      let component = this.createLink(this.model.findRole(link.source_id), this.model.findRole(link.target_id), link.object_id);
      component.loadJSON(link);
    }

    this.isModified = false;
  }

  /**
   * Toggle a clicked role button on a toolbar,
   *  so it is always only one clicked.
   * @param event
   * @param role  role clicked
   */
  toggleClickedRoleBtn(event, role) {
    if (this.roleClicked == role) {
      this.roleClicked = null;
    } else {
      this.roleClicked = role;
    }

    event.stopPropagation()
  }

  /**
   * Unclicks all role buttons on a toolbar.
   * @param event
   */
  untoggleClickedRoleBtn(event) {
    this.roleClicked = null;
  }

  /**
   * Returns a component class of a role based on a class name.
   * @param {string} name   name of a role class
   * @returns {any}         component class
   */
  static getClassFromName(name: string) {
    switch (name) {
      case 'DesktopComponent': {
        return DesktopComponent;
      }
      case 'MobileComponent': {
        return MobileComponent;
      }
      case 'RouterComponent': {
        return RouterComponent;
      }
      case 'ServerComponent': {
        return ServerComponent
      }
      default: {
        console.log('Function getClassFromName didn\'t match any classes for class name: ' + name + '.');
        return undefined;
      }
    }
  }

  /**
   * Validates if two components can be linked.
   * @param comp1
   * @param comp2
   * @returns {boolean}   true, if components can be linked; false, otherwise
   */
  isValidForLinking(comp1, comp2): boolean {
    let isOk = true;

    for (let link of this.model.links) {
      if ((link.getModel().source_id == comp1.getModel().object_id && link.getModel().target_id == comp2.getModel().object_id)
        || (link.getModel().source_id == comp2.getModel().object_id && link.getModel().target_id == comp1.getModel().object_id)) {
        isOk = false;
        break;
      }
    }

    return !(comp1 instanceof HostComponent && comp2 instanceof HostComponent) && isOk;
  }

  /**
   * Validates if a created graph is created correctly and nothing is missing.
   * @returns {boolean}   true, if graph is valid; false, otherwise
   */
  isValidForSaving(): boolean {
    let isValid = true;

    for (let role of this.model.roles) {
      if (!role.getModel().isValid()) {
        isValid = false;
        role.isValid = false;
        this.errorMessage = 'One or more roles are missing some required fields or some of them have failed validation.'
      }
    }

    return isValid;
  }
}
