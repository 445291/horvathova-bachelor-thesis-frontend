import {async, TestBed} from "@angular/core/testing";
import {DatabaseService} from "../shared/services/database.service";
import {TestModule} from "../test.module";
import {HttpClientModule} from "@angular/common/http";
import {TabCanvasComponent} from "./tab-canvas.component";
import {RoleDetailComponent} from "../sidebar/role-detail.component";
import {RouterComponent} from "../element/role/router/router";
import {DesktopComponent} from "../element/role/host/desktop";
import {LinkComponent} from "../element/link/link.component";
import {ServerComponent} from "../element/role/host/server";

describe('TabCanvasComponent', () => {
  let fixture;
  let component;
  let databaseServiceSpy;
  let tabElement;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('DatabaseService',
      ['saveGraph']);

    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        TestModule,
        HttpClientModule,
      ],
      providers: [
        TabCanvasComponent,
        {provide: DatabaseService, useValue: spy},
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TabCanvasComponent);
    component = fixture.componentInstance;
    databaseServiceSpy = TestBed.get(DatabaseService);
    component.ngOnInit();
  });

  it('should create a role detail component', async(() => {
    expect(component.roleDetailComponent instanceof RoleDetailComponent).toBe(true)
  }));

  it('should create component', async(() => {
    component.roleClicked = 'RouterComponent';
    component.getCanvasMouseX = function () {
      return 100;
    };
    component.getCanvasMouseY = function () {
      return 200;
    };
    component.create({x: 100, y: 200});
    expect(component.selectedElement instanceof RouterComponent).toBe(true);
  }));

  it('should create role component', async(() => {
    expect(component.createComponent(RouterComponent) instanceof RouterComponent).toBe(true);
  }));

  it('should create link component', async(() => {
    let from = component.createComponent(RouterComponent);
    let to = component.createComponent(DesktopComponent);
    expect(component.createLink(from, to) instanceof LinkComponent).toBe(true);
  }));

  it('should select component', async(() => {
    let role = component.createComponent(RouterComponent);
    role.getModel.id = 1;
    component.selectComponent(role);
    expect(component.selectedElement instanceof RouterComponent).toBe(true);
    expect(component.selectedElement.getModel.id).toBe(1);
  }));

  it('should call saveGraph in database service', async(() => {
    component.isValidForSaving = function () {
      return true;
    };
    spyOn(window, 'prompt').and.returnValue('graph_name');
    component.save();

    expect(databaseServiceSpy.saveGraph.calls.count()).toBe(1, 'saveGraph should be called exactly once');
    expect(databaseServiceSpy.saveGraph.calls.mostRecent().args[0]).toBe('graph_name');
  }));

  it('should call load graph', async(() => {
    let data = {
      id: 1,
      name: "graph_name",
      roles: [{
        class_name: "RouterComponent",
        object_id: 0,
        x: 1000,
        y: 800,
        data_name: "router1",
        data_ip: "1.1.1.1",
        data_prefix: "24",
        data_image_id: "",
        data_note: "",
        resourcetype: "Router"
      },
        {
          class_name: "RouterComponent",
          object_id: 1,
          x: 300,
          y: 300,
          data_name: "router2",
          data_ip: "2.2.2.2",
          data_prefix: "24",
          data_image_id: "",
          data_note: "",
          resourcetype: "Router"
        }],
      links: [{object_id: 2, source_id: 0, target_id: 1, data_note: ""}]
    };
    component.load(data);

    expect(component.filename).toEqual('graph_name');
  }));

  it('should resolve isValidForLinking correctly', async(() => {
    let role1 = component.createComponent(RouterComponent);
    let role2 = component.createComponent(RouterComponent);

    expect(component.isValidForLinking(role1, role2)).toBe(true);

    role1 = component.createComponent(DesktopComponent);
    role2 = component.createComponent(ServerComponent);

    expect(component.isValidForLinking(role1, role2)).toBe(false);
  }));

  it('should resolve isValidForSaving correctly', async(() => {
    let role1 = component.createComponent(RouterComponent);
    let role2 = component.createComponent(RouterComponent);

    expect(component.isValidForSaving()).toBe(false);

    role1.getModel().x = 100;
    role1.getModel().y = 100;
    role1.getModel().data_name = 'graph1';
    role1.getModel().data_ip = '1.1.1.1';
    role1.getModel().data_prefix = '24';
    role2.getModel().x = 100;
    role2.getModel().y = 100;
    role2.getModel().data_name = 'graph2';
    role2.getModel().data_ip = '2.2.2.2';
    role2.getModel().data_prefix = '24';

    fixture.detectChanges();

    expect(component.isValidForSaving()).toBe(true);
  }));
});
