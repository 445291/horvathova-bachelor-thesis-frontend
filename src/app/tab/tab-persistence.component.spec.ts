import {async, TestBed} from '@angular/core/testing';
import {DatabaseService} from "../shared/services/database.service";
import {TestModule} from "../test.module";
import {TabPersistenceComponent} from "./tab-persistence.component";
import {HttpClientModule} from "@angular/common/http";
import {Subject} from "rxjs/Subject";
import {Graph} from "../shared/models/graph.model";

describe('TabPersistenceComponent', () => {
  let fixture;
  let component;
  let databaseServiceSpy;
  let tabElement;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('DatabaseService',
      ['getAllGraphsDataObservable', 'getGraphDataObservable', 'getJsonDownloadGraphDataObservable', 'getAllGraphs',
        'loadGraph', 'downloadJSON', 'saveGraph', 'deleteGraph', 'renameGraph']);

    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        TestModule,
        HttpClientModule,
      ],
      providers: [
        TabPersistenceComponent,
        {provide: DatabaseService, useValue: spy},
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TabPersistenceComponent);
    component = fixture.componentInstance;
    databaseServiceSpy = TestBed.get(DatabaseService);
    tabElement = fixture.nativeElement.querySelector('.database-tab');
  });

  it('should contain a table of graphs', async(() => {
    const subject = new Subject();
    databaseServiceSpy.getAllGraphsDataObservable.and.returnValue(subject);
    component.ngOnInit();

    expect(databaseServiceSpy.getAllGraphs.calls.count()).toBe(1, 'getAllGraphs should be called exactly once');

    let graphs: Graph[] = [];
    const graph1 = new Graph();
    graph1.name = 'graph_name_1';
    const graph2 = new Graph();
    graph2.name = 'graph_name_2';

    graphs.push(graph1);
    graphs.push(graph2);
    subject.next(graphs);

    fixture.detectChanges();
    const content = tabElement.textContent;

    expect(content.includes(graph1.name)).toBe(true);
    expect(content.includes(graph2.name)).toBe(true);
  }));

  it('should call loadGraph in database service', async(() => {
    component.load('test_name');

    expect(databaseServiceSpy.loadGraph.calls.count()).toBe(1, 'loadGraph should be called exactly once');
    expect(databaseServiceSpy.loadGraph.calls.mostRecent().args[0]).toBe('test_name');
  }));

  it('should call deleteGraph in database service', async(() => {
    spyOn(window, 'confirm').and.returnValue(true);
    component.delete('test_name');

    expect(databaseServiceSpy.deleteGraph.calls.count()).toBe(1, 'deleteGraph should be called exactly once');
    expect(databaseServiceSpy.deleteGraph.calls.mostRecent().args[0]).toBe('test_name');
  }));

  it('should call downloadJSON in database service', async(() => {
    component.downloadJSON('test_name');

    expect(databaseServiceSpy.downloadJSON.calls.count()).toBe(1, 'downloadJSON should be called exactly once');
    expect(databaseServiceSpy.downloadJSON.calls.mostRecent().args[0]).toBe('test_name');
  }));

  it('should call rename with proper args in database service', async(() => {
    spyOn(window, 'prompt').and.returnValue('new_name');
    component.rename('old_name');

    expect(databaseServiceSpy.renameGraph.calls.count()).toBe(1, 'rename should be called exactly once');
    expect(databaseServiceSpy.renameGraph.calls.mostRecent().args[0]).toBe('old_name');
    expect(databaseServiceSpy.renameGraph.calls.mostRecent().args[1]).toBe('new_name');
  }));

  it('should call getAllGraphs in database service', async(() => {
    component.refreshList();

    expect(databaseServiceSpy.getAllGraphs.calls.count()).toBe(1, 'getAllGraphs should be called exactly once');
  }));

  it('should load JSON file', async(() => {
    let spyFileReader = jasmine.createSpyObj('FileReader', ['readAsText']);
    spyOn(component, 'getFileReader').and.returnValue(spyFileReader);

    let testFile = {
      testKey: 'testValue'
    };
    component.handleFileInput({
      item: function (index) {
        return testFile;
      }
    });

    expect(spyFileReader.readAsText.calls.count()).toBe(1);
    expect(spyFileReader.readAsText.calls.mostRecent().args[0]).toBe(testFile);

    const subjectSpy = jasmine.createSpyObj('Subject', ['next']);
    databaseServiceSpy.getGraphDataObservable.and.returnValue(subjectSpy);

    let testJSON = {testResult: "testResult"};
    spyFileReader.result = JSON.stringify(testJSON);
    spyFileReader.onload(null);

    expect(databaseServiceSpy.getGraphDataObservable.calls.count()).toBe(1);
    expect(subjectSpy.next.calls.count()).toBe(1);
    expect(JSON.stringify(subjectSpy.next.calls.mostRecent().args[0])).toBe(JSON.stringify(testJSON));
  }));
});
