import {Component, OnInit} from "@angular/core";
import {MatTableDataSource} from "@angular/material";

import {DatabaseService} from "../shared/services/database.service";
import {Graph} from "../shared/models/graph.model";

/**
 * A component for a persistence tab.
 */
@Component({
  templateUrl: './tab-persistence.component.html',
  styleUrls: ['./tab-persistence.component.css']
})

export class TabPersistenceComponent implements OnInit {

  constructor(private databaseService: DatabaseService) {
  }

  dataSource = new MatTableDataSource<Graph>();
  displayedColumns = ['name', 'actions'];
  filenameInput: string = '';
  isHidden = true;
  errorMessage: string = null;

  /**
   * Observer for allGraphsData of DatabaseService.
   */
  ngOnInit() {
    this.databaseService.getAllGraphsDataObservable().subscribe(data => {
      let graphs: Graph[] = data as Graph[];
      this.dataSource.data = graphs.reverse();
    });

    this.refreshList();
  }

  /**
   * Loads a graph.
   * Uses DatabaseService.
   * @param {string} filename     name of a graph to be loaded
   */
  load(filename: string) {
    this.databaseService.loadGraph(filename);
  }

  /**
   * After extra confirmation by prompt window, deletes a graph.
   * Uses DatabaseService.
   * @param {string} filename     name of a graph to be deleted
   */
  delete(filename: string) {
    if (window.confirm('Are you sure you want to delete this graph?')) {
      this.databaseService.deleteGraph(filename);
    }
  }

  /**
   * Downloades a graph as JSON.
   * Uses DatabaseService.
   * @param {string} filename     name of a graph to be downloaded
   */
  downloadJSON(filename: string) {
    this.databaseService.downloadJSON(filename);
  }

  /**
   * Asks for a new name
   *  and delegates both old and new names to the DatabaseService method 'renameGraph'.
   * When renaming ssuccessful refreshes list of graphs,
   *  otherwise sets error message.
   * @param {string} filename
   */
  rename(filename: string) {
    let newFilename = window.prompt('Please enter new graph name:');

    if (newFilename != null && newFilename != '') {
      this.databaseService.renameGraph(filename, newFilename,
        function () {
          this.refreshList();
        }.bind(this),
        function (response) {
          this.errorMessage = response.error.message;
        }.bind(this));
    }
  }

  /**
   * Refreshes a list of graphs in a persistence table.
   * Sets error message, if is not able to connect to the server.
   */
  refreshList() {
    this.databaseService.getAllGraphs(function (response) {
      this.errorMessage = 'Unable to connect to the REST API server.';
    }.bind(this));
  }

  /**
   * Accepts file fo a loading.
   * Publishes them into graphData in DatabaseService, where they are dealed with.
   * @param {FileList} files
   */
  handleFileInput(files: FileList) {
    let file = files.item(0);
    let fileReader = this.getFileReader();

    fileReader.onload = (e) => {
      this.databaseService.getGraphDataObservable().next(JSON.parse(fileReader.result));
    };
    fileReader.readAsText(file);
  }

  getFileReader() {
    return new FileReader();
  }

  /**
   * Finds hidden 'fileInput' in HTMl and clicks on it to trigger upload.
   */
  upload() {
    document.getElementById('fileInput').click();
  }

  dismissError() {
    this.errorMessage = null;
  }
}
