import {HostComponent} from "./host";
import {Component} from "@angular/core";

/**
 * A class for a server component.
 */
@Component({
  selector: 'svg:svg[app-role]',
  templateUrl: '../role.component.html',
  styleUrls: ['../role.component.css']
})
export class ServerComponent extends HostComponent {

  getImage() {
    if (this.isHighlighted) {
      return "assets/img/networkServerHighlighted.svg"
    } else {
      return "assets/img/networkServer.svg"
    }
  }

  /**
   * Returns a title of a server role for a sidebar.
   * @returns {string}    title
   */
  getTitle() {
    return 'server';
  }

  getClassName(): string {
    return 'ServerComponent';
  }
}
