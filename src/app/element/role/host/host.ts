import {RoleComponent} from "../role.component";
import {HostModel} from "../../../shared/models/host.model";
import {Component} from "@angular/core";

/**
 * A class for a host components.
 */
export abstract class HostComponent extends RoleComponent {
  private model: HostModel = new HostModel();

  getModel() {
    return this.model;
  }

  getResourceType(): string {
    return 'Host';
  }
}
