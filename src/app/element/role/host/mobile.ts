import {HostComponent} from "./host";
import {Component} from "@angular/core";

/**
 * A class for a mobile component.
 */
@Component({
  selector: 'svg:svg[app-role]',
  templateUrl: '../role.component.html',
  styleUrls: ['../role.component.css']
})
export class MobileComponent extends HostComponent {

  getImage() {
    if (this.isHighlighted) {
      return "assets/img/mobileHighlighted.svg"
    } else {
      return "assets/img/mobile.svg"
    }
  }

  /**
   * Returns a title of a mobile role for a sidebar.
   * @returns {string}    title
   */
  getTitle() {
    return 'mobile';
  }

  getClassName(): string {
    return 'MobileComponent';
  }
}
