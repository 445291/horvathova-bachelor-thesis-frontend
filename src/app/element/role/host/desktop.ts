import {HostComponent} from "./host";
import {Component} from "@angular/core";

/**
 * A class for a desktop component.
 */
@Component({
  selector: 'svg:svg[app-role]',
  templateUrl: '../role.component.html',
  styleUrls: ['../role.component.css']
})
export class DesktopComponent extends HostComponent {

  getImage() {
    if (this.isHighlighted) {
      return "assets/img/computerHighlighted.svg"
    } else {
      return "assets/img/computer.svg"
    }
  }

  /**
   * Returns a title of a desktop role for a sidebar.
   * @returns {string}    title
   */
  getTitle() {
    return 'desktop';
  }

  getClassName(): string {
    return 'DesktopComponent';
  }
}
