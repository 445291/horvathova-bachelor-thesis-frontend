import {TestModule} from "../../test.module";
import {async, TestBed} from "@angular/core/testing";
import {RoleComponent} from "./role.component";
import {RouterComponent} from "./router/router";

describe('RoleComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        TestModule,
      ],
      providers: [
        RoleComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RouterComponent);
    component = fixture.componentInstance;
    component.getModel().x = 100;
    component.getModel().y = 100;
    component.ngOnInit();
  });

  it('should move by', async(() => {
    component.moveBy(20, 20);
    expect(component.isInMotion).toBe(true);
    expect(component.getModel().x).toEqual(120);
    expect(component.getModel().y).toEqual(120);
  }));

  it('should start connecting', async(() => {
    component.callbackStartConnecting = function () {
    };
    component.startConnecting({
      stopPropagation: function () {
      }
    });
    expect(component.isClicked).toBe(true);
  }));

  it('should stop connecting', async(() => {
    component.stopConnecting();
    expect(component.isClicked).toBe(false);
  }));

  it('should return centres', async(() => {
    expect(component.getCenterX()).toEqual(100);
    expect(component.getCenterY()).toEqual(100);
  }));

  it('should set centres', async(() => {
    component.setCenter(50, 50);
    expect(component.getModel().x).toEqual(50);
    expect(component.getModel().y).toEqual(50);
  }));

  it('should prepare JSON', async(() => {
    component.setCenter(50, 50);
    expect(component.getModel().x).toEqual(50);
    expect(component.getModel().y).toEqual(50);
  }));
});


