import {RoleComponent} from "../role.component";
import {RouterModel} from "../../../shared/models/router.model";
import {Component} from "@angular/core";

/**
 * A class for a router component.
 */
@Component({
  selector: 'svg:svg[app-role]',
  templateUrl: '../role.component.html',
  styleUrls: ['../role.component.css']
})
export class RouterComponent extends RoleComponent {
  private model: RouterModel = new RouterModel();

  getImage() {
    if (this.isHighlighted) {
      return "assets/img/networkModemHighlighted.svg"
    } else {
      return "assets/img/networkModem.svg"
    }
  }

  /**
   * Returns a title of a router role for a sidebar.
   * @returns {string}    title
   */
  getTitle() {
    return 'router';
  }

  getModel() {
    return this.model;
  }

  getResourceType(): string {
    return 'Router';
  }

  getClassName(): string {
    return 'RouterComponent';
  }
}
