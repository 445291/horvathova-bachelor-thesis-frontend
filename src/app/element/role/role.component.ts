import {Component, OnInit} from '@angular/core';

import {LinkComponent} from "../link/link.component";
import {ElementComponent} from "../element.component";

/**
 * A component of role nodes.
 */
export abstract class RoleComponent extends ElementComponent implements OnInit {
  ref: any;

  origX: number;
  origY: number;
  radius = 45;

  callbackMouseDown: Function;
  callbackMouseUp: Function;
  callbackStartConnecting: Function;
  callbackDelete: Function;

  outboundLinks: LinkComponent[] = [];
  inboundLinks: LinkComponent[] = [];

  isInMotion = false;
  isClicked = false;
  linkingDisallowed = false;
  isValid = true;

  ngOnInit() {
    this.origX = this.getModel().x;
    this.origY = this.getModel().y;
  }

  mouseUp(event) {
    let click = false;

    if ((Math.abs(this.origX - event.x) < 5)
      && (Math.abs(this.origY - event.y) < 5)) {
      click = true;
    }

    this.isInMotion = false;
    this.callbackMouseUp(this, event, click);
    event.stopPropagation();
  }

  mouseDown(event) {
    this.origX = event.x;
    this.origY = event.y;
    this.callbackMouseDown(this, event);
    event.stopPropagation();
  }

  moveBy(dx, dy) {
    this.isInMotion = true;
    this.setCenter(this.getModel().x + dx, this.getModel().y + dy);
  }

  /**
   * Updates all inbound and outbound links of a node.
   */
  updateLinks() {
    for (let link of this.outboundLinks) {
      link.updateStart(this.getCenterX(), this.getCenterY(), this);
    }

    for (let link of this.inboundLinks) {
      link.updateEnd(this.getCenterX(), this.getCenterY(), this);
    }
  }

  addOutboundLink(linkComp) {
    this.outboundLinks.push(linkComp);
    linkComp.updateStart(this.getCenterX(), this.getCenterY(), this);
  }

  addInboundLink(linkComp) {
    this.inboundLinks.push(linkComp);
    linkComp.updateEnd(this.getCenterX(), this.getCenterY(), this);
  }

  removeObject() {
    this.callbackDelete(this);
  }

  deleteClicked(event) {
    this.removeObject();
    event.stopPropagation();
  }

  startConnecting(event) {
    this.isClicked = true;
    this.callbackStartConnecting(this, event);
    event.stopPropagation();
  }

  stopConnecting() {
    this.isClicked = false;
  }

  getCenterX() {
    return this.getModel().x;
  }

  getCenterY() {
    return this.getModel().y;
  }

  getRadius() {
    return this.radius;
  }

  /**
   * Sets centre of a node for given coordinates.
   * Checks if coordinates are within defined canvas,
   *  if not, resets a node at the nearest border.
   * @param x   coordinate x
   * @param y   coordinate y
   */
  setCenter(x, y) {
    this.getModel().x = x;
    this.getModel().y = y;

    if (this.getModel().x < this.radius) {
      this.getModel().x = this.radius
    }
    if (this.getModel().y < this.radius) {
      this.getModel().y = this.radius
    }
    if (this.getModel().x > 3200 - this.radius) {
      this.getModel().x = 3200 - this.radius;
    }
    if (this.getModel().y > 1800 - this.radius) {
      this.getModel().y = 1800 - this.radius;
    }

    this.updateLinks();
  }

  toJSON() {
    let out = super.toJSON();
    out['resourcetype'] = this.getResourceType();

    return out;
  }

  /**
   * Return a resource type of roles for the backend.
   * @returns {string} resource type - 'Host' for HostComponent; 'Router' for RouterComponent
   */
  abstract getResourceType(): string;

  /**
   * Returns a image for a node of a particular role.
   * Differentiate whether it is highlighted or not.
   * @returns {string}    svg Papirus image
   */
  abstract getImage(): string;
}
