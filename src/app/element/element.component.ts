/**
 * A component for elements of graph.
 */
export abstract class ElementComponent {
  isHighlighted: boolean = false;

  highlight() {
    this.isHighlighted = true;
  }

  dehighlight() {
    this.isHighlighted = false;
  }

  /**
   * Prepares graph data as JSON. Uses elements models.
   * @returns JSON of a graph
   */
  toJSON() {
    let out = {
      class_name: this.getClassName(),
    };

    this.getModel().saveToDict(out);

    return out;
  }

  /**
   * Parse data from a JSON. Uses elements models.
   * @param data  JSON data of a graph
   */
  loadJSON(data) {
    this.getModel().loadFromDict(data);
  }

  abstract getTitle(): string;

  /**
   * Returns a model of a element.
   * @returns {any} model
   */
  abstract getModel(): any;

  abstract getClassName(): string;
}
