import {Component} from '@angular/core';

import * as svgIntersections from "svg-intersections";

import {LinkModel} from "../../shared/models/link.model";
import {ElementComponent} from "../element.component";

/**
 * A component for a link.
 */
@Component({
  selector: 'svg:svg[app-link]',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css']
})

export class LinkComponent extends ElementComponent {
  private model: LinkModel = new LinkModel();

  ref: any;

  callbackClick: Function;
  callbackDelete: Function;

  isHidden: boolean = false;

  d = 'M0,0 L0,0';

  x1 = 0;
  y1 = 0;
  originRole = null;

  x2 = 0;
  y2 = 0;
  targetRole = null;

  click(event) {
    this.callbackClick(this, event);
    event.stopPropagation();
  }

  removeLink() {
    this.ref.destroy();
  }

  deleteClicked(event) {
    this.callbackDelete(this);
    this.removeLink();
    event.stopPropagation();
  }

  getMiddleX() {
    return ((this.x1 + this.x2) / 2)
  }

  getMiddleY() {
    return ((this.y1 + this.y2) / 2)
  }

  /**
   * This method updates a string for "d" parameter of a path for a link.
   * Uses svg-intersections library for calculating intersections of a link and nodes it connects,
   *  so always the shortest path is rendered.
   */
  updateD() {
    this.isHidden = false;

    let newX1 = this.x1;
    let newY1 = this.y1;
    let newX2 = this.x2;
    let newY2 = this.y2;

    // calculates intersection with a node it starts from
    if (this.originRole) {
      let intersections = svgIntersections.intersect(
        svgIntersections.shape('line', {
          x1: this.x1,
          y1: this.y1,
          x2: this.x2,
          y2: this.y2
        }),
        svgIntersections.shape('circle', {
          cx: this.originRole.getModel().x,
          cy: this.originRole.getModel().y,
          r: this.originRole.radius
        })
      );

      if (intersections.points.length > 0) {
        let vectX = intersections.points[0].x - this.x2;
        let vectY = intersections.points[0].y - this.y2;
        newX1 = this.x2 + LinkComponent.getVectEndForLinkX(vectX, vectY);
        newY1 = this.y2 + LinkComponent.getVectEndForLinkY(vectX, vectY);
      }
    }

    // calculates intersection with a node it ends in
    if (this.targetRole) {
      let intersections = svgIntersections.intersect(
        svgIntersections.shape('line', {
          x1: this.x1,
          y1: this.y1,
          x2: this.x2,
          y2: this.y2
        }),
        svgIntersections.shape('circle', {
          cx: this.targetRole.getModel().x,
          cy: this.targetRole.getModel().y,
          r: this.targetRole.radius
        })
      );

      if (intersections.points.length > 0) {
        let vectX = intersections.points[0].x - this.x1;
        let vectY = intersections.points[0].y - this.y1;

        newX2 = this.x1 + LinkComponent.getVectEndForLinkX(vectX, vectY);
        newY2 = this.y1 + LinkComponent.getVectEndForLinkY(vectX, vectY);
      }
    } else {
      let vectX = this.x2 - this.x1;
      let vectY = this.y2 - this.y1;

      // when creating a link, it can not be displayed over a node it starts from
      this.isHidden = Math.sqrt(vectX * vectX + vectY * vectY) < 45;

      if (Math.sqrt(vectX * vectX + vectY * vectY) < 90) {
        let vectLength = Math.sqrt(vectX * vectX + vectY * vectY);
        vectX /= vectLength;
        vectY /= vectLength;
        vectX *= 90;
        vectY *= 90;
      }

      newX2 = this.x1 + LinkComponent.getVectEndForLinkX(vectX, vectY);
      newY2 = this.y1 + LinkComponent.getVectEndForLinkY(vectX, vectY);
    }

    this.d = 'M' + newX1 + ',' + newY1 + ' L' + newX2 + ',' + newY2;
  }

  static getVectEndForLinkX(vectX, vectY) {
    let vectLength = Math.sqrt(vectX * vectX + vectY * vectY);
    vectX /= vectLength;
    vectX *= vectLength - 14;

    return vectX;
  }

  static getVectEndForLinkY(vectX, vectY) {
    let vectLength = Math.sqrt(vectX * vectX + vectY * vectY);
    vectY /= vectLength;
    vectY *= vectLength - 14;

    return vectY;
  }

  /**
   * Updates a start of a link.
   * @param x     coordinate x
   * @param y     coordinates y
   * @param role  node it starts from
   */
  updateStart(x, y, role) {
    this.x1 = x;
    this.y1 = y;
    this.originRole = role;
    this.updateD();
  }

  /**
   * Updates an end of a link.
   * @param x     coordinate x
   * @param y     coordinates y
   * @param role  node it ends in
   */
  updateEnd(x, y, role) {
    this.x2 = x;
    this.y2 = y;
    this.targetRole = role;
    this.updateD();
  }

  getTitle() {
    return 'LINK';
  }

  getModel() {
    return this.model;
  }


  getClassName(): string {
    return 'LinkComponent';
  }
}

