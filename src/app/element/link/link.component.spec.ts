import {TestModule} from "../../test.module";
import {async, TestBed} from "@angular/core/testing";
import {LinkComponent} from "./link.component";

describe('LinkComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        TestModule,
      ],
      providers: [
        LinkComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LinkComponent);
    component = fixture.componentInstance;

  });

  it('should calculate middles correctly', async(() => {
    component.x1 = 200;
    component.x2 = 100;
    component.y1 = 50;
    component.y2 = 60;

    expect(component.getMiddleX()).toEqual(150);
    expect(component.getMiddleY()).toEqual(55);
  }));
});
