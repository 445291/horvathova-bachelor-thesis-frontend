import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {TabPersistenceComponent} from "./tab/tab-persistence.component";
import {TestModule} from "./test.module";
import {TabCanvasComponent} from "./tab/tab-canvas.component";

describe('AppComponent', () => {
  let fixture;
  let component;
  let last_but_one;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        TestModule
      ],
      providers: [
        AppComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    component.addCanvasTab('test_graph');
    last_but_one = component.components.length - 1;

  });

  it('should create a persistence tab', async(() => {
    expect(component.components[0].instance instanceof TabPersistenceComponent).toBe(true)
  }));

  it('should add a canvas tab', async(() => {
    let new_canvas_tab = component.components[last_but_one].instance;
    expect(new_canvas_tab instanceof TabCanvasComponent).toBe(true);
    expect(new_canvas_tab.filename).toEqual('test_graph');
  }));

  it('should change a tab', async(() => {
    component.onTabChange(0);

    expect(component.selectedTab).toBe(0);
    expect(component.components[component.selectedTab].instance instanceof TabPersistenceComponent).toBe(true);

    component.onTabChange(last_but_one);

    expect(component.selectedTab).toBe(last_but_one);
    expect(component.components[component.selectedTab].instance instanceof TabCanvasComponent).toBe(true);
  }));

  it('should delete a canvas tab', async(() => {
    let array_length_before = component.components.length;
    let deleted_tab = component.components[last_but_one];
    component.deleteTab(null, last_but_one);

    expect(component.components.length).toEqual(array_length_before - 1);
    expect(component.components.indexOf(deleted_tab)).toBe(-1);
  }));
});
