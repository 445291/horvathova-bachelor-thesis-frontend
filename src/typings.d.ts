/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

declare module 'svg-intersections';

declare module 'file-saver';

declare module 'svg-pan-zoom';
