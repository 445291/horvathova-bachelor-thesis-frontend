# FrontEnd

This project was generated with [*Angular CLI*](https://github.com/angular/angular-cli) version 1.6.3. It is a frontend part of an implementation for my bachelors theses **Network Topology Editor**.

## Preparation
First install *Node.js* and *npm* on your computer. Download this project and inside your folder for this project run `npm install`.

## Running development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

If you want to run against development REST API, change the URL in `frontend\src\app\shared\services\database.service.ts.`
 
## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running production server

After building the project, when folder `dist/` is created, start **Express server** that will serve the folder on port 80 with `npm start`.

## Running unit tests

Run `ng test` to execute the unit tests via [*Karma*](https://karma-runner.github.io).
